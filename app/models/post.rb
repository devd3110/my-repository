class Post < ActiveRecord::Base
	# adding many to one assignment also we destroy comment the movement post is deleted.
	has_many :comments, dependent: :destroy
	validates_presence_of :title
	validates_presence_of :body
end
